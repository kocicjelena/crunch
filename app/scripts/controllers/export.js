'use strict';

angular.module('crunchinatorApp.controllers')
.controller('MyCtrl',  [
    '$scope', '$stateParams', '$state', 
    function MyCtrl($scope,$stateParams,$state) {
        var section = angular.element('#exporttpl');
        var body = angular.element('body');

        $scope.showPage = 'export.tpl';
        $scope.isMobile = Browser.isMobile.any();
        $scope.Analytics = Analytics;

        $scope.navigate = function(page) {
            Analytics.event('Navigation', 'Click', page);
            if (body.scrollTop() === section.offset().top) {
                $scope.showPage = page;
            } else {
                angular.element('body').animate({scrollTop: section.offset().top}, 'slow', function() {
                    $scope.showPage = page;
                    $scope.$digest();
                });
            }
        };
    }
]);
